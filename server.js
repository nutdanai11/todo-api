const express = require('express')
const app = express()
let todos = [
    {
        name : 'Nut',
        id : 1
    },
    {
        name : 'Stitch',
        id : 2
    }
]

app.get('/todos',(req, res) => {
res.send(todos)
})
app.post('/todos', (req,res) => {
let newTodo = {
    name: "Read a book",
    id: 3
}
todos.push(newTodo)
res.status(201).send()
}) 
app.listen(3000, () =>{
console.log('TODO API Starts at port 3000')
})